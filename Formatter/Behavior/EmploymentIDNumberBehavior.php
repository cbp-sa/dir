<?php

namespace BureauHouse\Modules\Directory\Formatter\Behavior;

use BureauHouse\Formatter\Behavior\AbstractBehavior;
use BureauHouse\Formatter\SearchFilter;

final class EmploymentIDNumberBehavior extends AbstractBehavior
{
    protected function getDefaultParameters()
    {
        return [
            'SortBy'    => 'RecordDate',
            'SortOrder' => 'desc',
            'UseMuni'   => 'True',
            'Merge'     => 'Yes',
        ];
    }

    protected function getFilters()
    {
        return [
            (new SearchFilter())->setField('Score')->isGreater(20)
        ];
    }
}
