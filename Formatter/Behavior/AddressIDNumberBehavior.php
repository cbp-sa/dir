<?php

namespace BureauHouse\Modules\Directory\Formatter\Behavior;

use BureauHouse\Formatter\Behavior\AbstractBehavior;
use BureauHouse\Formatter\SearchFilter;

final class AddressIDNumberBehavior extends AbstractBehavior
{
    protected function getDefaultParameters()
    {
        return [
            'SortBy'        => 'Score',
            'SortOrder'     => 'desc',
        ];
    }

    protected function getFilters()
    {
        return [
            (new SearchFilter())->setField('Score')->isGreater(30),
            (new SearchFilter())->setField('RecordDate')->isGreater('1900-01-01'),
        ];
    }
}
