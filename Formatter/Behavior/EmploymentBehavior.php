<?php

namespace BureauHouse\Modules\Directory\Formatter\Behavior;

use BureauHouse\Formatter\Behavior\AbstractBehavior;
use BureauHouse\Formatter\SearchFilter;

final class EmaploymentBehavior extends AbstractBehavior
{
    /*public function format()
    {
        return $this->setFormattedParameters(
            [
                'SortBy'    => 'RecordDate',
                'SortOrder' => 'desc',
                'UseMuni'   => 'True',
                'Merge' => 'Yes',
            ]
        );
    }*/

    protected function getFilters()
    {
        return [
            (new SearchFilter())->setField('Score')->isGreater(20)
        ];
    }
}
