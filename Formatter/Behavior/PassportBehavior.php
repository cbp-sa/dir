<?php

namespace BureauHouse\Modules\Directory\Formatter\Behavior;

use BureauHouse\Formatter\Behavior\AbstractBehavior;
use BureauHouse\Formatter\SearchFilter;

final class PassportBehavior extends AbstractBehavior
{
    protected function getDefaultParameters()
    {
        return [
            'Reference'       => $this->getParameter('Reference'),
            'PassportNumber'  => $this->getParameter('Passport'),
        ];
    }

    protected function getFilters()
    {
        return [

        ];
    }
}
