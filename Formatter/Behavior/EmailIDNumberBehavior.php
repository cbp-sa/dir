<?php

namespace BureauHouse\Modules\Directory\Formatter\Behavior;

use BureauHouse\Formatter\Behavior\AbstractBehavior;
use BureauHouse\Formatter\SearchFilter;

final class EmailIDNumberBehavior extends AbstractBehavior
{
    protected function getDefaultParameters()
    {
        return [
            'SortBy'        => 'Score',
            'SortOrder'     => 'desc',
        ];
    }

    protected function getFilters()
    {
        return [
            (new SearchFilter())->setField('Score')->isGreater(20)
        ];
    }
}
