<?php

namespace BureauHouse\Modules\Directory\Formatter\Behavior;

use BureauHouse\Formatter\Behavior\AbstractBehavior;
use BureauHouse\Formatter\SearchFilter;

final class EmailBehavior extends AbstractBehavior
{
    protected function getDefaultParameters()
    {
        return [
            'Reference'     => $this->getParameter('Reference'),
            'EmailAddress'  => $this->getParameter($this->getType()),
            'AddNames'      => 'True',
        ];
    }

    protected function getFilters()
    {
        return [
            (new SearchFilter())->setField('Score')->isGreater(20)
        ];
    }
}
