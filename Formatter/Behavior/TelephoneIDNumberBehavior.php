<?php

namespace BureauHouse\Modules\Directory\Formatter\Behavior;

use BureauHouse\Formatter\Behavior\AbstractBehavior;
use BureauHouse\Formatter\SearchFilter;

final class TelephoneIDNumberBehavior extends AbstractBehavior
{
    protected function getFilters()
    {
        return [
            (new SearchFilter())->setField('LatestStatus')->notEqual('Historical'),
            (new SearchFilter())->setField('LatestDate')->isGreater('1900-01-01'),
            (new SearchFilter())->setField('Score')->isGreater(20),
        ];
    }

    protected function getDefaultParameters()
    {
        return [
            'ShowLinks'     => 'True',
            'SortBy'        => 'Score',
            'SortOrder'     => 'desc',
            'BusinessName'  => 'true',
            'ShowLinks'     => 'true',
            'Merge'         => 'false',
            'Summary'       => 'true',
        ];
    }
}
