<?php

namespace BureauHouse\Modules\Directory\Formatter\Behavior;

use BureauHouse\Formatter\Behavior\AbstractBehavior;

final class CompanyBehavior extends AbstractBehavior
{
    protected function getDefaultParameters()
    {
        return [
            'Reference'     => $this->getParameters('Reference'),
            'RegistrationNumber'  => $this->getParameters('Company'),
        ];
    }
}
