<?php

namespace BureauHouse\Modules\Directory\Formatter\Behavior;

use BureauHouse\Formatter\Behavior\AbstractBehavior;
use BureauHouse\Formatter\SearchFilter;

final class TelephoneBehavior extends AbstractBehavior
{
    protected function getFilters()
    {
        return [
            (new SearchFilter())->setField('Surname')->setValue($this->getParameter('Surname')),
        ];
    }

    protected function getDefaultParameters()
    {
        return [
            'TelNumber'         => str_replace('.', '%', $this->getParameter('Telephone')),
            'Reference'         => $this->getParameter('Reference'),
            'ReferenceNumber'   => $this->getParameter('Reference'),
            'ShowLinks'         => 'True',
            'AddNames'          => 'True',
        ];
    }
}
