<?php

namespace BureauHouse\Modules\Directory\Formatter\Behavior;

use BureauHouse\Formatter\Behavior\AbstractBehavior;

final class CompanyIDNumberBehavior extends AbstractBehavior
{
    protected function getDefaultParameters()
    {
        return [
            'SortBy' => 'AppointmentDate',
            'SortOrder' => 'desc',
        ];
    }
}
