<?php

namespace BureauHouse\Modules\Directory\Database\Seeders;

use BureauHouse\Modules\Core\Database\Seeders\AbstractSectionInputTableSeeder;
use BureauHouse\Modules\Directory\Entities\Section;
use BureauHouse\Modules\Directory\Entities\Input;

class SectionInputTableSeeder extends AbstractSectionInputTableSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this
            ->tryInsert($this->getValues($this->getSectionByName(Section::EMAIL), $this->getInputByName(Input::EMAIL)))
        ;
    }
}
