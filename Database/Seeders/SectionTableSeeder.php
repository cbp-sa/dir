<?php

namespace BureauHouse\Modules\Directory\Database\Seeders;

use BureauHouse\Modules\Core\Database\Seeders\AbstractSectionTableSeeder;
use BureauHouse\Modules\Directory\Entities\Section;

class SectionTableSeeder extends AbstractSectionTableSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this
            ->insertIdNumberSection()
            ->insertIdNumberSectionDataLoader()
        ;
    }

    private function insertIdNumberSection()
    {
        return $this
            ->tryInsert($this->getValues(Section::NAMES, 'people/list', 'Personlist', true))
            ->tryInsert($this->getValues(Section::DECEASED, 'deceased/list', 'Deceasedlist', true))
            ->tryInsert($this->getValues(Section::DEBT_REVIEW, 'debtreview/list', 'DebtReviewlist'))
            ->tryInsert($this->getValues(Section::TRACE_LOCATOR, 'tracelocator/list', true))
            ->tryInsert($this->getValues(Section::DEFAULTS, 'default/list', 'Defaultslist'))
            ->tryInsert($this->getValues(Section::DEEDS, 'deeds/list', 'DeedsRecordlist'))
            ->tryInsert($this->getValues(Section::TELEPHONE, 'telephone/list', 'Telephonelist', true))
            ->tryInsert($this->getValues(Section::EMAIL, 'emailaddress/list', 'Emaillist', true))
            ->tryInsert($this->getValues(Section::EMPLOYMENT, 'employment/list', 'Employmentlist'))
            ->tryInsert($this->getValues(Section::CIPC_DIRECTORS, 'cipcdirectors/list', 'CIPCDirectorlist'))
            ->tryInsert($this->getValues(Section::ADDRESS, 'address/list', 'Addresslist', true))
            ->tryInsert($this->getValues(Section::RELATIVES, 'relatives/list', 'Relativeslist'))
            ->tryInsert($this->getValues(Section::RELATIVES_OTHER, 'relatives/otherlinks', 'Relativesotherlinks'))
            ->tryInsert($this->getValues(Section::SAFPS, 'safps/list'))
            ->tryInsert($this->getValues(Section::SAFPS_PROTECTIVE, 'safpsprotective/list'))
            ->tryInsert($this->getValues(Section::CIPC_ENTERPRISES, 'cipcenterprises/list', 'CIPCEnterpriselist'))
            ->tryInsert($this->getValues(Section::CIPC_AUDITORS, 'cipcauditors/list', 'CIPCAuditorlist'))
            ->tryInsert($this->getValues(Section::STRIKE_DATES, 'strikedates/list'))
        ;
    }

    private function insertIdNumberSectionDataLoader()
    {
        return $this

        ;
    }

    private function insertNameSection()
    {
        return $this
            ->tryInsert($this->getValues(Section::PERSON, 'people/list', 'Personlist'))
        ;
    }
}
