<?php

namespace BureauHouse\Modules\Directory\Database\Seeders;

use BureauHouse\Modules\Core\Database\Seeders\AbstractProductTableSeeder;
use BureauHouse\Modules\Directory\Entities\Product;

class ProductTableSeeder extends AbstractProductTableSeeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this
            ->setPosition(0)
            ->tryInsert($this->getValues(Product::DIRECTORY))
        ;
    }
}
