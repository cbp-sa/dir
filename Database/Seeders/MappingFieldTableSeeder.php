<?php

namespace BureauHouse\Modules\Directory\Database\Seeders;

use BureauHouse\Modules\Core\Database\Seeders\AbstractMappingFieldTableSeeder;
use BureauHouse\Modules\Directory\Entities\Field;
use BureauHouse\Modules\Directory\Entities\Mapping;
use BureauHouse\Modules\Directory\Entities\Module;

class MappingFieldTableSeeder extends AbstractMappingFieldTableSeeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this
            ->insertPeople()
            ->insertTelephone()
            ->insertAddress()
            ->insertDebtReviews()
            ->insertDeceased()
            ->insertDefault()
            ->insertEmail()
            ->insertEmployer()
            ->insertCIPCAuditor()
            ->insertCIPCDirector()
            ->insertCIPCEntreprise()
            ->insertRelatives()
        ;
    }

    private function insertPeople()
    {
        return $this
            ->resetPosition()
            ->setMapping($this->getMappingByName(Mapping::PEOPLE))
            ->setModule($this->getModuleByName(Module::ID_NUMBER))
            ->tryInsert($this->getValues($this->getFieldByName(Field::ID_NUMBER), 'ID Number'))
            ->tryInsert($this->getValues($this->getFieldByName(Field::DATE_OF_BIRTH)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::AGE)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::SURNAME)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::FIRST_NAME)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::MAIDEN_NAME)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::PASSPORT), 'Passport Number'))
            ->tryInsert($this->getValues($this->getFieldByName(Field::MARITAL_STATUS)))
            ->resetPosition()
            ->setModule($this->getModuleByName(Module::NAMES))
            ->tryInsert($this->getValues($this->getFieldByName(Field::ID_NUMBER), 'ID Number'))
            ->tryInsert($this->getValues($this->getFieldByName(Field::PASSPORT_NUMBER)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::DATE_OF_BIRTH)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::AGE)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::SURNAME)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::FIRST_NAME)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::MAIDEN_NAME)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::COUNTRY)))
            ->resetPosition()
            ->setModule($this->getModuleByName(Module::PASSPORT))
            ->tryInsert($this->getValues($this->getFieldByName(Field::ID_NUMBER), 'ID Number'))
            ->tryInsert($this->getValues($this->getFieldByName(Field::PASSPORT_NUMBER), 'Passport Number'))
            ->tryInsert($this->getValues($this->getFieldByName(Field::DATE_OF_BIRTH)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::AGE)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::SURNAME)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::FIRST_NAME)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::MAIDEN_NAME)))
        ;
    }

    private function insertTelephone()
    {
        return $this
            ->resetPosition()
            ->setMapping($this->getMappingByName(Mapping::TELEPHONES))
            ->setModule($this->getModuleByName(Module::ID_NUMBER))
            ->tryInsert($this->getValues($this->getFieldByName(Field::TEL_NUMBER), 'Number'))
            ->tryInsert($this->getValues($this->getFieldByName(Field::LINKS)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::TEL_TYPE), 'Type'))
            ->tryInsert($this->getValues($this->getFieldByName(Field::NETWORK)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::BUSINESS_NAME)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::LATEST_DATE)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::SCORE)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::FIRST_STATUS), Field::STATUS))
            ->resetPosition()
            ->setModule($this->getModuleByName(Module::TELEPHONE))
            ->tryInsert($this->getValues($this->getFieldByName(Field::DATE_OF_BIRTH)))
            //->tryInsert($this->getValues($this->getFieldByName(Field::FULL_NAMES)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::SURNAME)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::TEL_NUMBER)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::FIRST_STATUS), Field::STATUS))
            ->tryInsert($this->getValues($this->getFieldByName(Field::LATEST_DATE)))
        ;
    }

    private function insertAddress()
    {
        return $this
            ->resetPosition()
            ->setModule($this->getModuleByName(Module::ID_NUMBER))
            ->setMapping($this->getMappingByName(Mapping::OUTPUT_ADDRESSES))
            ->tryInsert($this->getValues($this->getFieldByName(Field::BOX_LINE), 'Type'))
            ->tryInsert($this->getValues($this->getFieldByName(Field::STREET_LINE)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::BUILDING_LINE)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::ORIGINAL_ADDRESS), Field::ADDRESS))
            ->tryInsert($this->getValues($this->getFieldByName(Field::LATEST_DATE), Field::CONFIRMED_DATE))
            ->tryInsert($this->getValues($this->getFieldByName(Field::SCORE)))
        ;
    }

    private function insertDebtReviews()
    {
        return $this
            ->resetPosition()
            ->setModule($this->getModuleByName(Module::ID_NUMBER))
            ->setMapping($this->getMappingByName(Mapping::DEBT_REVIEW_RECORDS))
            ->tryInsert($this->getValues($this->getFieldByName(Field::DEBT_COUNCELLOR), 'Debt Counsellor'))
            ->tryInsert($this->getValues($this->getFieldByName(Field::DEBT_COUNCELLOR_CONTACT), Field::TELEPHONE))
            ->tryInsert($this->getValues($this->getFieldByName(Field::STATUS_CODE)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::RECORD_DATE), Field::DATE))
            ->tryInsert($this->getValues($this->getFieldByName(Field::STATUS_DESCRIPTION)))
        ;
    }

    private function insertDeceased()
    {
        return $this
            ->resetPosition()
            ->setModule($this->getModuleByName(Module::ID_NUMBER))
            ->setMapping($this->getMappingByName(Mapping::DECEASED_RECORDS))
            ->tryInsert($this->getValues($this->getFieldByName(Field::DATE_OF_DEATH)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::EXECUTOR_NAME), 'Executor'))
            ->tryInsert($this->getValues($this->getFieldByName(Field::EXECUTOR_ADDRESS)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::ESTATE_NUMBER)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::PLACE_OF_DEATH)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::SURVIVING_SPOUSE_ID_NUMBER), 'Spouse Details'))
        ;
    }

    private function insertDefault()
    {
        return $this
            ->resetPosition()
            ->setModule($this->getModuleByName(Module::ID_NUMBER))
            ->setMapping($this->getMappingByName(Mapping::DEFAULT_RECORDS))
            ->tryInsert($this->getValues($this->getFieldByName('Subscriber')))
            ->tryInsert($this->getValues($this->getFieldByName('ContactInformation')))
            ->tryInsert($this->getValues($this->getFieldByName('AccountNumber')))
            ->tryInsert($this->getValues($this->getFieldByName(Field::REFERENCE)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::AMOUNT)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::STATUS_CODE)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::COMMENTS)))
        ;
    }

    private function insertEmail()
    {
        return $this
            ->resetPosition()
            ->setModule($this->getModuleByName(Module::ID_NUMBER))
            ->setMapping($this->getMappingByName(Mapping::EMAIL_ADDRESSES))
            ->tryInsert($this->getValues($this->getFieldByName(Field::EMAIL_ADDRESS)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::LATEST_DATE), Field::CONFIRMED_DATE))
            ->tryInsert($this->getValues($this->getFieldByName(Field::SCORE)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::LATEST_STATUS), Field::STATUS))
        ;
    }

    private function insertEmployer()
    {
        return $this
            ->resetPosition()
            ->setModule($this->getModuleByName(Module::ID_NUMBER))
            ->setMapping($this->getMappingByName(Mapping::EMPLOYERS))
            ->tryInsert($this->getValues($this->getFieldByName(Field::EMPLOYER_NAME), 'Company_Name'))
            ->tryInsert($this->getValues($this->getFieldByName(Field::EMPLOYER_REGISTRATION_NUMBER), 'Registration_Number'))
            ->tryInsert($this->getValues($this->getFieldByName(Field::OCCUPATION)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::CONTACT_PERSON), 'Contact Person/Phone'))
            ->tryInsert($this->getValues($this->getFieldByName(Field::LATEST_DATE)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::SCORE)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::LATEST_STATUS), Field::STATUS))
        ;
    }

    private function insertCIPCAuditor()
    {
        return $this
            ->resetPosition()
            ->setModule($this->getModuleByName(Module::ID_NUMBER))
            ->setMapping($this->getMappingByName(Mapping::CIPC_AUDITORS))
            ->tryInsert($this->getValues($this->getFieldByName(Field::AUDITOR_NAME)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::AUDITOR_STATUS)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::PROFESSION_NUMBER)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::BUSINESS_ADDRESS_LINE1)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::POSTAL_ADDRESS_LINE1)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::REGISTER_ENTRY_DATE)))
        ;
    }

    private function insertCIPCDirector()
    {
        return $this
            ->resetPosition()
            ->setModule($this->getModuleByName(Module::ID_NUMBER))
            ->setMapping($this->getMappingByName(Mapping::CIPC_DIRECTORS))
            ->tryInsert($this->getValues($this->getFieldByName(Field::ID_NUMBER)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::PERSON_NAME)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::DIRECTOR_STATUS)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::DIRECTOR_TYPE)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::APPOINTMENT_DATE)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::RESIGNATION_DATE)))
        ;
    }

    private function insertCIPCEntreprise()
    {
        return $this
            ->resetPosition()
            ->setModule($this->getModuleByName(Module::ID_NUMBER))
            ->setMapping($this->getMappingByName(Mapping::CIPC_ENTERPRISES))
            ->tryInsert($this->getValues($this->getFieldByName(Field::REGISTRATION_NUMBER)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::ENTERPRISE_NAME)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::STATUS_CODE)))
            ->tryInsert($this->getValues($this->getFieldByName(Field::STATUS)))
        ;
    }

    private function insertRelatives()
    {
        return $this
            ->resetPosition()
            ->setModule($this->getModuleByName(Module::ID_NUMBER))
            ->setMapping($this->getMappingByName(Mapping::RELATIVES))
            ->tryInsert($this->getValues($this->getFieldByName(Field::RELATIVES_ID), 'ID_Number'))
            ->tryInsert($this->getValues($this->getFieldByName(Field::FIRST_NAMES), 'Relative'))
            ->tryInsert($this->getValues($this->getFieldByName(Field::SURNAME), 'sname'))
            ->tryInsert($this->getValues($this->getFieldByName(Field::LINK_TYPE), 'Link'))
        ;
    }
}
