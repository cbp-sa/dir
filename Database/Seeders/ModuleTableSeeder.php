<?php

namespace BureauHouse\Modules\Directory\Database\Seeders;

use BureauHouse\Modules\Core\Database\Seeders\AbstractModuleTableSeeder;
use BureauHouse\Modules\Directory\Entities\Module;

class ModuleTableSeeder extends AbstractModuleTableSeeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this
            ->tryInsert($this->getValues(Module::ID_NUMBER, 'fa-id-card'))
            ->tryInsert($this->getValues(Module::PASSPORT, 'fa-passport'))
            ->tryInsert($this->getValues(Module::TELEPHONE, 'fa-phone'))
            ->tryInsert($this->getValues(Module::NAMES, 'fa-users'))
            ->tryInsert($this->getValues(Module::ADDRESS, 'fa-map-marker-alt'))
            ->tryInsert($this->getValues(Module::EMAIL, 'fa-envelope'))
            ->tryInsert($this->getValues(Module::COMPANY, 'fa-building'))
            ->tryInsert($this->getValues(Module::HISTORY, 'fa-history'))
        ;
    }
}
