<?php

namespace BureauHouse\Modules\Directory\Database\Seeders;

use BureauHouse\Modules\Core\Database\Seeders\AbstractInputTableSeeder;
use BureauHouse\Modules\Core\Entities\Type;
use BureauHouse\Modules\Core\Models\SelectValue;
use BureauHouse\Modules\Core\Models\SelectOption;
use BureauHouse\Modules\Directory\Entities\Input;

class InputTableSeeder extends AbstractInputTableSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $selectValue = new SelectValue();
        $selectValue
            ->addOption((new SelectOption())->setValue('both')->setText('Both'))
            ->addOption((new SelectOption())->setValue('male')->setText('Male'))
            ->addOption((new SelectOption())->setValue('female')->setText('Female'))
            ->setSelected('both')
        ;
        $this
            ->insertIdNumber()
            ->insertName()
            ->insertAddress()
            ->insertTelephone()

            ->tryInsert($this->getValues(Input::REFERENCE, 'Reference', '[a-zA-Z]{2,}'))
            ->tryInsert($this->getValues(Input::PASSPORT, 'Passport', '^[A-Z][\d]{8}$'))
            ->tryInsert($this->getValues(Input::DOB_FROM, 'DOB FROM', '\d{4}\/\d{2}\/\d{2}'))
            ->tryInsert($this->getValues(Input::DOB_TO, 'DOB TO', '\d{4}\/\d{2}\/\d{2}'))
            ->tryInsert($this->setRangeValues(
                $this->getValues(Input::AGE_RANGE, 'Age Range', null, Type::RANGE),
                18,
                150
            ))
            ->tryInsert($this->setSelectValues(
                $this->getValues(Input::GENDER, 'Gender', null, Type::SELECT),
                $selectValue
            ))
            ->tryInsert($this->getValues(Input::NUMBER, 'Number', '\d+'))
            ->tryInsert($this->getValues(Input::EMAIL, 'Email Address', null, Type::EMAIL))

            ->tryInsert($this->getValues(Input::COMPANY_SEARCH_OPTION, 'Company Search Option'))
            ->tryInsert($this->getValues(Input::COMPANY, 'Company Search Criteria'))
        ;
    }

    private function insertName()
    {
        return $this
            ->tryInsert($this->getValues(Input::SURNAME, 'Surname', '[a-zA-Z\W]+'))
            ->tryInsert($this->getValues(Input::NAMES, 'Names', '[a-zA-Z\W]+'))
        ;
    }

    private function insertAddress()
    {
        return $this
            ->tryInsert($this->getValues(Input::SUBURB, 'Suburb'))
            ->tryInsert($this->getValues(Input::STREET_NAME, 'Street Name'))
            ->tryInsert($this->getValues(Input::STREET_NAME, 'Add text to search for Street Name'))
        ;
    }

    private function insertTelephone()
    {
        return $this
            ->tryInsert($this->getValues(Input::TELEPHONE, 'Telephone', '^\d{10}$'))
        ;
    }

    private function insertIdNumber()
    {
        return $this
            ->tryInsert($this->getValues(Input::ID_NUMBER, 'ID number', '^\d{13}$', Type::NUMBER))
        ;
    }
}
