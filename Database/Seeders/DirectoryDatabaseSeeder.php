<?php

namespace BureauHouse\Modules\Directory\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DirectoryDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $this->call(ProductTableSeeder::class);
        $this->call(ModuleTableSeeder::class);
        $this->call(ModuleProductTableSeeder::class);
        $this->call(InputTableSeeder::class);
        $this->call(ModuleInputTableSeeder::class);
        $this->call(DataLoaderTableSeeder::class);
        $this->call(SectionTableSeeder::class);
        $this->call(SectionInputTableSeeder::class);
        $this->call(ModuleSectionTableSeeder::class);
        $this->call(MappingTableSeeder::class);
        $this->call(FieldTableSeeder::class);
        $this->call(MappingFieldTableSeeder::class);
    }
}
