<?php

namespace BureauHouse\Modules\Directory\Database\Seeders;

use BureauHouse\Modules\Core\Database\Seeders\AbstractModuleInputTableSeeder;
use BureauHouse\Modules\Directory\Entities\Input;
use BureauHouse\Modules\Directory\Entities\Module;

class ModuleInputTableSeeder extends AbstractModuleInputTableSeeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this
            ->insertIdNumber()
            ->insertPassport()
            ->insertTelephone()
            ->insertName()
            ->insertAddress()
            ->insertEmail()
            ->insertCompany()
        ;
    }

    private function insertIdNumber()
    {
        return $this
            ->resetPosition()
            ->tryInsert($this->getValues($this->getModuleByName(Module::ID_NUMBER), $this->getInputByName(Input::REFERENCE)))
            ->tryInsert($this->getValues($this->getModuleByName(Module::ID_NUMBER), $this->getInputByName(Input::ID_NUMBER)))
        ;
    }

    private function insertPassport()
    {
        return $this
            ->resetPosition()
            ->tryInsert($this->getValues($this->getModuleByName(Module::PASSPORT), $this->getInputByName(Input::REFERENCE)))
            ->tryInsert($this->getValues($this->getModuleByName(Module::PASSPORT), $this->getInputByName(Input::PASSPORT)))
        ;
    }

    private function insertTelephone()
    {
        return $this
            ->resetPosition()
            ->tryInsert($this->getValues($this->getModuleByName(Module::TELEPHONE), $this->getInputByName(Input::REFERENCE)))
            ->tryInsert($this->getValues($this->getModuleByName(Module::TELEPHONE), $this->getInputByName(Input::TELEPHONE)))
            ->tryInsert($this->getValues($this->getModuleByName(Module::TELEPHONE), $this->getInputByName(Input::SURNAME), false))
        ;
    }

    private function insertName()
    {
        return $this
            ->resetPosition()
            ->tryInsert($this->getValues($this->getModuleByName(Module::NAMES), $this->getInputByName(Input::REFERENCE)))
            ->tryInsert($this->getValues($this->getModuleByName(Module::NAMES), $this->getInputByName(Input::SURNAME)))
            ->tryInsert($this->getValues($this->getModuleByName(Module::NAMES), $this->getInputByName(Input::NAMES)))
            ->tryInsert($this->getValues($this->getModuleByName(Module::NAMES), $this->getInputByName(Input::DOB_FROM), false))
            ->tryInsert($this->getValues($this->getModuleByName(Module::NAMES), $this->getInputByName(Input::DOB_TO), false))
            ->tryInsert($this->getValues($this->getModuleByName(Module::NAMES), $this->getInputByName(Input::AGE_RANGE)))
            ->tryInsert($this->getValues($this->getModuleByName(Module::NAMES), $this->getInputByName(Input::GENDER)))
        ;
    }

    private function insertAddress()
    {
        return $this
            ->resetPosition()
            ->tryInsert($this->getValues($this->getModuleByName(Module::ADDRESS), $this->getInputByName(Input::REFERENCE)))
            ->tryInsert($this->getValues($this->getModuleByName(Module::ADDRESS), $this->getInputByName(Input::SUBURB)))
            ->tryInsert($this->getValues($this->getModuleByName(Module::ADDRESS), $this->getInputByName(Input::STREET_NAME)))
            ->tryInsert($this->getValues($this->getModuleByName(Module::ADDRESS), $this->getInputByName(Input::NUMBER)))
        ;
    }

    private function insertEmail()
    {
        return $this
            ->resetPosition()
            ->tryInsert($this->getValues($this->getModuleByName(Module::EMAIL), $this->getInputByName(Input::REFERENCE)))
            ->tryInsert($this->getValues($this->getModuleByName(Module::EMAIL), $this->getInputByName(Input::EMAIL)))
            ->tryInsert($this->getValues($this->getModuleByName(Module::EMAIL), $this->getInputByName(Input::SURNAME), false))
        ;
    }

    private function insertCompany()
    {
        return $this
            ->resetPosition()
            ->tryInsert($this->getValues($this->getModuleByName(Module::COMPANY), $this->getInputByName(Input::REFERENCE)))
            ->tryInsert($this->getValues($this->getModuleByName(Module::COMPANY), $this->getInputByName(Input::COMPANY_SEARCH_OPTION)))
            ->tryInsert($this->getValues($this->getModuleByName(Module::COMPANY), $this->getInputByName(Input::COMPANY)))
        ;
    }
}
