<?php

namespace BureauHouse\Modules\Directory\Database\Seeders;

use BureauHouse\Modules\Core\Database\Seeders\AbstractDataLoaderTableSeeder;
use BureauHouse\Modules\Directory\Entities\Section;

class DataLoaderTableSeeder extends AbstractDataLoaderTableSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this
            ->tryInsert($this->getValues(Section::ADDRESS, 'dataloader/loadaddress', 'NewDataLoaderloadaddress'))
            ->tryInsert($this->getValues(Section::DECEASED, 'dataloader/loaddeceased', 'NewDataLoaderloaddeceased'))
            ->tryInsert($this->getValues(Section::EMAIL, 'dataloader/loademailaddress', 'NewDataLoaderloademailaddress'))
            ->tryInsert($this->getValues(Section::EMPLOYMENT, 'dataloader/loademployer', 'NewDataLoaderloademployer'))
            ->tryInsert($this->getValues(Section::TELEPHONE, 'dataloader/loadtelephone', 'NewDataLoaderloadtelephone'))
        ;
    }
}
