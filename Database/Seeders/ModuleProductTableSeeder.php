<?php

namespace BureauHouse\Modules\Directory\Database\Seeders;

use BureauHouse\Modules\Core\Database\Seeders\AbstractModuleProductTableSeeder;
use BureauHouse\Modules\Directory\Entities\Module;
use BureauHouse\Modules\Directory\Entities\Product;

class ModuleProductTableSeeder extends AbstractModuleProductTableSeeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this
            ->resetPosition()
            ->tryInsert($this->getValues($this->getModuleByName(Module::ID_NUMBER), $this->getProductByName(Product::DIRECTORY)))
            ->tryInsert($this->getValues($this->getModuleByName(Module::PASSPORT), $this->getProductByName(Product::DIRECTORY)))
            ->tryInsert($this->getValues($this->getModuleByName(Module::TELEPHONE), $this->getProductByName(Product::DIRECTORY)))
            ->tryInsert($this->getValues($this->getModuleByName(Module::NAMES), $this->getProductByName(Product::DIRECTORY)))
            ->tryInsert($this->getValues($this->getModuleByName(Module::ADDRESS), $this->getProductByName(Product::DIRECTORY)))
            ->tryInsert($this->getValues($this->getModuleByName(Module::EMAIL), $this->getProductByName(Product::DIRECTORY)))
            ->tryInsert($this->getValues($this->getModuleByName(Module::COMPANY), $this->getProductByName(Product::DIRECTORY)))
            ->tryInsert($this->getValues($this->getModuleByName(Module::HISTORY), $this->getProductByName(Product::DIRECTORY)))
        ;
    }
}
