<?php

namespace BureauHouse\Modules\Directory\Database\Seeders;

use BureauHouse\Modules\Core\Database\Seeders\AbstractModuleSectionTableSeeder;
use BureauHouse\Modules\Directory\Entities\Module;
use BureauHouse\Modules\Directory\Entities\Section;

class ModuleSectionTableSeeder extends AbstractModuleSectionTableSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this
            ->insertIdNumber()
            ->insertPassport()
            ->insertTelephone()
            ->insertEmail()
            ->insertCompany()
        ;
    }

    private function insertIdNumber()
    {
        return $this
            ->tryInsert($this->getValues($this->getModuleByName(Module::ID_NUMBER), $this->getSectionByName(Section::NAMES)))
            ->tryInsert($this->getValues($this->getModuleByName(Module::ID_NUMBER), $this->getSectionByName(Section::DECEASED)))
            ->tryInsert($this->getValues($this->getModuleByName(Module::ID_NUMBER), $this->getSectionByName(Section::DEBT_REVIEW)))
            ->tryInsert($this->getValues($this->getModuleByName(Module::ID_NUMBER), $this->getSectionByName(Section::TRACE_LOCATOR)))
            ->tryInsert($this->getValues($this->getModuleByName(Module::ID_NUMBER), $this->getSectionByName(Section::DEFAULTS)))
            ->tryInsert($this->getValues($this->getModuleByName(Module::ID_NUMBER), $this->getSectionByName(Section::DEEDS)))
            ->tryInsert($this->getValues($this->getModuleByName(Module::ID_NUMBER), $this->getSectionByName(Section::TELEPHONE)))
            ->tryInsert($this->getValues($this->getModuleByName(Module::ID_NUMBER), $this->getSectionByName(Section::EMAIL)))
            ->tryInsert($this->getValues($this->getModuleByName(Module::ID_NUMBER), $this->getSectionByName(Section::ADDRESS)))
            ->tryInsert($this->getValues($this->getModuleByName(Module::ID_NUMBER), $this->getSectionByName(Section::EMPLOYMENT)))
            ->tryInsert($this->getValues($this->getModuleByName(Module::ID_NUMBER), $this->getSectionByName(Section::RELATIVES)))
            ->tryInsert($this->getValues($this->getModuleByName(Module::ID_NUMBER), $this->getSectionByName(Section::RELATIVES_OTHER)))
        ;
    }

    private function insertPassport()
    {
        return $this
            ->tryInsert($this->getValues($this->getModuleByName(Module::PASSPORT), $this->getSectionByName(Section::NAMES)))
        ;
    }

    private function insertTelephone()
    {
        return $this
            ->tryInsert($this->getValues($this->getModuleByName(Module::TELEPHONE), $this->getSectionByName(Section::TELEPHONE)))
        ;
    }

    private function insertEmail()
    {
        return $this
            ->tryInsert($this->getValues($this->getModuleByName(Module::EMAIL), $this->getSectionByName(Section::EMAIL)))
        ;
    }

    private function insertCompany()
    {
        return $this
            ->tryInsert($this->getValues($this->getModuleByName(Module::COMPANY), $this->getSectionByName(Section::CIPC_ENTERPRISES)))
            ->tryInsert($this->getValues($this->getModuleByName(Module::COMPANY), $this->getSectionByName(Section::CIPC_AUDITORS)))
            ->tryInsert($this->getValues($this->getModuleByName(Module::COMPANY), $this->getSectionByName(Section::CIPC_DIRECTORS)))
        ;
    }
}
