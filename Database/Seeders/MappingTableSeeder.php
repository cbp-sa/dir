<?php

namespace BureauHouse\Modules\Directory\Database\Seeders;

use BureauHouse\Modules\Core\Database\Seeders\AbstractMappingTableSeeder;
use BureauHouse\Modules\Core\Entities\Type;
use BureauHouse\Modules\Directory\Entities\Mapping;
use BureauHouse\Modules\Directory\Entities\Section;

class MappingTableSeeder extends AbstractMappingTableSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this
            ->tryInsert($this->getValues(Mapping::PEOPLE, Type::GRID, $this->getSectionByName(Section::NAMES)))
            ->tryInsert($this->getValues(Mapping::TELEPHONES, Type::GRID, $this->getSectionByName(Section::TELEPHONE)))
            ->tryInsert($this->getValues(Mapping::OUTPUT_ADDRESSES, Type::GRID, $this->getSectionByName(Section::ADDRESS)))
            ->tryInsert($this->getValues(Mapping::DEBT_REVIEW_RECORDS, Type::GRID, $this->getSectionByName(Section::DEBT_REVIEW)))
            ->tryInsert($this->getValues(Mapping::DECEASED_RECORDS, Type::GRID, $this->getSectionByName(Section::DECEASED)))
            ->tryInsert($this->getValues(Mapping::DEFAULT_RECORDS, Type::GRID, $this->getSectionByName(Section::DEFAULTS)))
            ->tryInsert($this->getValues(Mapping::EMAIL_ADDRESSES, Type::GRID, $this->getSectionByName(Section::EMAIL)))
            ->tryInsert($this->getValues(Mapping::EMPLOYERS, Type::GRID, $this->getSectionByName(Section::EMPLOYMENT)))
            ->tryInsert($this->getValues(Mapping::CIPC_AUDITORS, Type::GRID, $this->getSectionByName(Section::CIPC_AUDITORS)))
            ->tryInsert($this->getValues(Mapping::CIPC_DIRECTORS, Type::GRID, $this->getSectionByName(Section::CIPC_DIRECTORS)))
            ->tryInsert($this->getValues(Mapping::CIPC_ENTERPRISES, Type::GRID, $this->getSectionByName(Section::CIPC_ENTERPRISES)))
            ->tryInsert($this->getValues(Mapping::RELATIVES, Type::GRID, $this->getSectionByName(Section::RELATIVES)))
        ;
    }
}
