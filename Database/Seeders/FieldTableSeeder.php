<?php

namespace BureauHouse\Modules\Directory\Database\Seeders;

use BureauHouse\Modules\Core\Database\Seeders\AbstractFieldTableSeeder;
use BureauHouse\Modules\Directory\Entities\Field;

class FieldTableSeeder extends AbstractFieldTableSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this
            ->tryInsert($this->getValues(Field::TEL_NUMBER, 'Telephone Number'))
            ->tryInsert($this->getValues(Field::LINKS, 'Links'))
            ->tryInsert($this->getValues(Field::TEL_TYPE, 'TelType'))
            ->tryInsert($this->getValues(Field::NETWORK, 'Network'))
            ->tryInsert($this->getValues(Field::BUSINESS_NAME, 'Business Name'))
            ->tryInsert($this->getValues(Field::LATEST_DATE, 'Latest Date'))
            ->tryInsert($this->getValues(Field::SCORE, 'Score'))
            ->tryInsert($this->getValues(Field::FIRST_STATUS, 'First Status'))
            ->tryInsert($this->getValues(Field::ORIGINAL_ADDRESS, 'Original Address'))
            ->tryInsert($this->getValues(Field::TYPE, 'Type'))
            ->tryInsert($this->getValues(Field::BOX_LINE, 'Box Line'))
            ->tryInsert($this->getValues(Field::BUILDING_LINE, 'Building Line'))
            ->tryInsert($this->getValues(Field::STREET_LINE, 'Street Line'))
            ->tryInsert($this->getValues(Field::COUNTRY, 'Country'))

            ->tryInsert($this->getValues(Field::SUBSCRIBER, 'Subscriber'))
            ->tryInsert($this->getValues(Field::CONTACT_INFORMATION, 'Contact Information'))
            ->tryInsert($this->getValues(Field::ACCOUNT_NUMBER, 'Account Number'))
            ->tryInsert($this->getValues(Field::REFERENCE, 'Reference'))
            ->tryInsert($this->getValues(Field::AMOUNT, 'Amount'))
            ->tryInsert($this->getValues(Field::STATUS_CODE, 'Status Code'))
            ->tryInsert($this->getValues(Field::COMMENTS, 'Comments'))

            ->tryInsert($this->getValues(Field::EMAIL_ADDRESS, 'Email Address'))
            ->tryInsert($this->getValues(Field::LATEST_STATUS, 'Latest Status'))

            ->tryInsert($this->getValues(Field::ID_NUMBER, 'ID Number'))
            ->tryInsert($this->getValues(Field::PERSON_NAME, 'Name'))
            ->tryInsert($this->getValues(Field::DIRECTOR_STATUS, 'Director Status'))
            ->tryInsert($this->getValues(Field::DIRECTOR_TYPE, 'Director Type'))
            ->tryInsert($this->getValues(Field::APPOINTMENT_DATE, 'Appointment Date'))
            ->tryInsert($this->getValues(Field::RESIGNATION_DATE, 'Resignation Date'))

            ->tryInsert($this->getValues(Field::AUDITOR_NAME, 'Auditor Name'))
            ->tryInsert($this->getValues(Field::AUDITOR_STATUS, 'Auditor Status'))
            ->tryInsert($this->getValues(Field::PROFESSION_NUMBER, 'Profession Number'))
            ->tryInsert($this->getValues(Field::BUSINESS_ADDRESS_LINE1, 'Postal Address Line1'))
            ->tryInsert($this->getValues(Field::POSTAL_ADDRESS_LINE1, 'Postal Address Line1'))
            ->tryInsert($this->getValues(Field::REGISTER_ENTRY_DATE, 'Appointment Date'))

            ->tryInsert($this->getValues(Field::REGISTRATION_NUMBER, 'Registration NUmber'))
            ->tryInsert($this->getValues(Field::ENTERPRISE_NAME, 'Enterprise Name'))
            ->tryInsert($this->getValues(Field::STATUS_CODE, 'Status Code'))
            ->tryInsert($this->getValues(Field::STATUS, 'Status'))

            ->tryInsert($this->getValues(Field::DEBT_COUNCELLOR, 'Debt Counsellor'))
            ->tryInsert($this->getValues(Field::DEBT_COUNCELLOR_CONTACT, 'Debt Counsellor Contact'))
            ->tryInsert($this->getValues(Field::RECORD_DATE, 'Record Date'))
            ->tryInsert($this->getValues(Field::STATUS_DESCRIPTION, 'Status Description'))

            ->tryInsert($this->getValues(Field::DATE_OF_BIRTH, 'Date of birth'))
            ->tryInsert($this->getValues(Field::AGE, 'Age'))
            ->tryInsert($this->getValues(Field::SURNAME, 'Surname'))
            ->tryInsert($this->getValues(Field::FIRST_NAME, 'First Name'))
            ->tryInsert($this->getValues(Field::MAIDEN_NAME, 'Maiden Name'))
            ->tryInsert($this->getValues(Field::PASSPORT, 'Passport'))
            ->tryInsert($this->getValues(Field::PASSPORT_NUMBER, 'Passport Number'))
            ->tryInsert($this->getValues(Field::MARITAL_STATUS, 'Maritial Status'))

            ->tryInsert($this->getValues(Field::DATE_OF_DEATH, 'Date of death'))
            ->tryInsert($this->getValues(Field::EXECUTOR_NAME, 'Executor Name'))
            ->tryInsert($this->getValues(Field::EXECUTOR_ADDRESS, 'Executor Address'))
            ->tryInsert($this->getValues(Field::ESTATE_NUMBER, 'Estate Name'))
            ->tryInsert($this->getValues(Field::PLACE_OF_DEATH, 'Place of Birth'))
            ->tryInsert($this->getValues(Field::SURVIVING_SPOUSE_ID_NUMBER, 'Spouse Id Number'))

            ->tryInsert($this->getValues(Field::EMPLOYER_NAME, 'Employer Name'))
            ->tryInsert($this->getValues(Field::EMPLOYER_REGISTRATION_NUMBER, 'Employer Registration Number'))
            ->tryInsert($this->getValues(Field::OCCUPATION, 'Occupation'))
            ->tryInsert($this->getValues(Field::CONTACT_PERSON, 'Contact Person'))
            ->tryInsert($this->getValues(Field::LATEST_DATE, 'Latest Date'))
            ->tryInsert($this->getValues(Field::LATEST_STATUS, 'Latest Status'))

            ->tryInsert($this->getValues(Field::RELATIVES_ID, 'Relatives ID'))
            ->tryInsert($this->getValues(Field::FIRST_NAMES, 'First Names'))
            ->tryInsert($this->getValues(Field::SURNAME, 'Surname'))
            ->tryInsert($this->getValues(Field::LINK_TYPE, 'Link Type'))
        ;
    }
}
