<?php

namespace BureauHouse\Modules\Directory\Entities;

use BureauHouse\Modules\Core\Entities\Section as Entity;

class Section extends Entity
{
    const ADDRESS = 'Addresses';
    const CIPC_AUDITORS = 'CIPC Auditors';
    const CIPC_DIRECTORS = 'Directorship';
    const CIPC_ENTERPRISES = 'CIPC Enterprises';
    const DEBT_REVIEW = 'Debt Review Records';
    const DECEASED = 'Deceased Records';
    const DEFAULTS = 'Default Records';
    const DEEDS = 'Properties';
    const DISPUTES = 'Disputes';
    const EMAIL = 'Email Addresses';
    const EMPLOYMENT = 'Employers';
    const NAMES = 'Names';
    const RELATIVES = 'Possible Relatives';
    const RELATIVES_OTHER = 'Other Possible Links';
    const PERSON = 'Person';
    const POLITICIAN = 'Politician';
    const TELEPHONE = 'Telephone Numbers';
    const TRACE_LOCATOR = 'TraceLocator';
}
