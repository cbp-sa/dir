<?php

namespace BureauHouse\Modules\Directory\Entities;

use BureauHouse\Modules\Core\Entities\Mapping as Entity;

class Mapping extends Entity
{
    const CIPC_AUDITORS = 'CIPCAuditors';
    const CIPC_DIRECTORS = 'CIPCDirectors';
    const CIPC_ENTERPRISES = 'CIPCEnterprises';
    const DEBT_REVIEW_RECORDS = 'DebtReviewRecords';
    const DECEASED_RECORDS = 'DeceasedRecords';
    const DEFAULT_RECORDS = 'DefaultRecords';
    const EMAIL_ADDRESSES = 'EmailAddresses';
    const EMPLOYERS = 'Employers';
    const OUTPUT_ADDRESSES = 'OutputAddresses';
    const PEOPLE = 'People';
    const RELATIVES = 'Relatives';
    const TELEPHONES = 'Telephones';
}
