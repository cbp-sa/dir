<?php

namespace BureauHouse\Modules\Directory\Entities;

use BureauHouse\Modules\Core\Entities\Module as Entity;

class Module extends Entity
{
    const ADDRESS = 'Address';
    const COMPANY = 'Company';
    const EMAIL = 'Email';
    const HISTORY = 'History';
    const ID_NUMBER = 'ID Number';
    const PASSPORT = 'Passport';
    const TELEPHONE = 'Telephone';
    const NAMES = 'Names';
}
