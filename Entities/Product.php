<?php

namespace BureauHouse\Modules\Directory\Entities;

use BureauHouse\Modules\Core\Entities\Product as Entity;

class Product extends Entity
{
    const DIRECTORY = 'Directory';
}
