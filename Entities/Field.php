<?php

namespace BureauHouse\Modules\Directory\Entities;

use BureauHouse\Modules\Core\Entities\Field as Entity;

class Field extends Entity
{
    const ADDRESS = 'Address';
    const ACCOUNT_NUMBER = 'AccountNumber';
    const APPOINTMENT_DATE = 'AppointmentDate';
    const AGE = 'Age';
    const AUDITOR_NAME = 'AuditorName';
    const AUDITOR_STATUS = 'AuditorStatus';
    const AMOUNT = 'Amount';
    const BOX_LINE = 'BoxLine';
    const BUILDING_LINE = 'BuildingLine';
    const BUSINESS_NAME = 'BusinessName';
    const BUSINESS_ADDRESS_LINE1 = 'BusinessAddressLine1';
    const COMMENTS = 'Comments';
    const CONFIRMED_DATE = 'Confirmed Date';
    const CONTACT_PERSON = 'ContactPerson';
    const CONTACT_INFORMATION = 'ContactInformation';
    const COUNTRY = 'Country';
    const DATE = 'Date';
    const DATE_OF_BIRTH = 'DateOfBirth';
    const DATE_OF_DEATH = 'DateOfDeath';
    const DEBT_COUNCELLOR = 'DebtCouncillor';
    const DEBT_COUNCELLOR_CONTACT = 'DebtCouncillorContact';
    const DIRECTOR_STATUS = 'DirectorStatus';
    const DIRECTOR_TYPE = 'DirectorType';
    const EMAIL_ADDRESS = 'EmailAddress';
    const EMPLOYER_NAME = 'EmployerName';
    const EMPLOYER_REGISTRATION_NUMBER = 'EmployerRegistrationNumber';
    const ENTERPRISE_NAME = 'EnterpriseName';
    const ESTATE_NUMBER = 'EstateNumber';
    const EXECUTOR_ADDRESS = 'ExecutorAddress';
    const EXECUTOR_NAME = 'ExecutorName';
    const FIRST_NAME = 'FirstName';
    const FIRST_NAMES = 'FirstNames';
    const FIRST_STATUS = 'FirstStatus';
    const ID_NUMBER = 'IDNumber';
    const LATEST_DATE = 'LatestDate';
    const LATEST_STATUS = 'LatestStatus';
    const LINKS = 'Links';
    const LINK_TYPE = 'LinkType';
    const PASSPORT = 'Passport';
    const PASSPORT_NUMBER = 'InputIDNumber';
    const PERSON_NAME = 'PersonName';
    const PLACE_OF_DEATH = 'PlaceOfDeath';
    const OCCUPATION = 'Occupation';
    const PROFESSION_NUMBER = 'ProfessionNumber';
    const MAIDEN_NAME = 'MaidenName';
    const MARITAL_STATUS = 'MaritalStatus';
    const NETWORK = 'Network';
    const RECORD_DATE = 'RecordDate';
    const REFERENCE = 'Reference';
    const RELATIVES_ID = 'RelativesID';
    const REGISTER_ENTRY_DATE = 'RegisterEntryDate';
    const REGISTRATION_NUMBER = 'RegistrationNumber';
    const RESIGNATION_DATE = 'ResignationDate';
    const ORIGINAL_ADDRESS = 'OriginalAddress';
    const POSTAL_ADDRESS_LINE1 = 'PostalAddressLine1';
    const STREET_LINE = 'StreetLine';
    const SUBSCRIBER = 'Subscriber';
    const SCORE = 'Score';
    const STATUS = 'Status';
    const STATUS_CODE = 'StatusCode';
    const STATUS_DESCRIPTION = 'StatusDescription';
    const SURVIVING_SPOUSE_ID_NUMBER = 'SurvivingSpouseIDNumber';
    const SURNAME = 'Surname';
    const TEL_NUMBER = 'TelNumber';
    const TEL_TYPE = 'TelType';
    const TELEPHONE = 'Telephone';
    const TYPE = 'Type';

    protected $table = 'field';
    protected $fillable = ['name', 'description', 'input_id'];

    /**
     * {@inheritDoc}
     */
    protected $icon = 'fa-th';

    /**
     * Get the inputs for the field.
     *
     * @return Collection
     */
    public function input()
    {
        return $this->hasMany('BureauHouse\Modules\Core\Entities\Input', 'input_id');
    }
}
