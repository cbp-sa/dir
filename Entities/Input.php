<?php

namespace BureauHouse\Modules\Directory\Entities;

use BureauHouse\Modules\Core\Entities\Input as Entity;

class Input extends Entity
{
    const AGE_RANGE = 'Age Range';
    const COMPANY = 'Company';
    const COMPANY_SEARCH_OPTION = 'Company Search Option';
    const DOB = 'DOB';
    const DOB_FROM = 'DOB FROM';
    const DOB_TO = 'DOB TO';
    const EMAIL = 'Email';
    const GENDER = 'Gender';
    const ID_NUMBER = 'IDNumber';
    const REFERENCE = 'Reference';
    const PASSPORT = 'Passport';
    const NAMES = 'Names';
    const NUMBER = 'Number';
    const STREET_NAME = 'Street Name';
    const SUBURB = 'Suburb';
    const SURNAME = 'Surname';
    const TELEPHONE = 'Telephone';
}
